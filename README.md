# 2D Raycasting
A 2D Raycaster made entirely without 3rd-party libraries and written in C#. Position of Obstacles are predefined are not changeable while the process is running. The calculated data is stored in a CSV-formatted file and visualized with Javascript using [p5.js](https://p5js.org/) running locally on the users machine.
## Math
equation for Rays: $`\ r(x) = \frac {x-w} {\tan (\frac {a+o} {360}2\pi)}+h`$

start and ending point of obstacle:
$`W1(x1, y1)`$
$`W2(x2, y2)`$

equation for Obstacles: $`\ w(x) = mx +n`$
$`\ m = \frac {y1-y2} {x1 - x2}`$
$`\ n = y1 - mx2`$
## Intersection Detection
 Functions of Ray and Wall:
 $`\begin{vmatrix} r(x) = m1x+n1 \\ w(x) = m2x+n2 \end{vmatrix}`$

Insert Functions into a Equation:
$`\ m1x+n1 = m2x+n2`$

> We'll be using exemplary values to better illustrate the method

Solve the Equation:
$`\ 2x+4 = -3x+(-7)|-4`$
> negative values will be handled as + (-x) 

$`2x = -3x+(-11)|+3x`$

$`5x = -11|\div 5`$

$`x = -2.2`$

Insert the x value in r(x):
$`r(x)=y=2 \cdot (-2.2)+4 = -0.4`$

$`Intersection(x,y)=Intersection(-2.2,-0,4)`$
