using System;
using System.Collections.Generic;

namespace TwoDimensionalRaycasting
{
    public class RayEmitter
    {
        /// <summary>
        /// Class for  the "camera", emits rays which are Function classes 
        /// </summary>
        public struct EmitterProperties
        {
            public double X; 
            public double Y;
            public double Angle;
            public int Number;
            public double Fov;
        }
        
        public List<Function> Rays;

        public EmitterProperties Properties;
        public RayEmitter(double width, double height, double angle, double fov, int rayNumber) // TODO Throws NullReferenceException
        {
            Properties.X = width;
            Properties.Y = height;
            if (angle <= 0)
            {
                Properties.Angle = 1;
            }
            else
            {
                Properties.Angle = angle;
            } 
            Properties.Fov = fov;
            Properties.Number = rayNumber;
            var rayOffset = Properties.Fov / Properties.Number;
            var currentRayAngle = 0.0;
            
            for (var i = 0; i < rayNumber; i++)
            {
                if (Properties.Angle - (Properties.Fov / 2) < 0)
                {
                    currentRayAngle = 360 - (Properties.Fov / 2) + Properties.Angle + rayOffset;
                    if (currentRayAngle >= 360)
                    {
                        currentRayAngle = currentRayAngle - 359;
                    } 
                }
                else
                {
                    currentRayAngle = Properties.Angle - (Properties.Fov / 2) + rayOffset;
                }

                double y1 = (1 - Properties.X) / Math.Tan(((currentRayAngle / 360) * (Math.PI * 2))) + Properties.Y;
                double y2 = (2 - Properties.X) / Math.Tan(((currentRayAngle / 360) * (Math.PI * 2))) + Properties.Y;
                Function tempFunc = new Function(1, y1, 2, y2);
                Rays.Add(tempFunc);
            }
        }
    }

    public class Function 
    {
        
        // TODO add ToString() overwrite method
        /// <summary>
        /// A class representing a linear Function 
        /// </summary>
        public double M;
        public double N;
        public override string ToString()
        {
            return $"Y = {M}X + {N}";
        }

        public Function(double x1, double y1, double x2, double y2)
        {
            M = (y1 - y2) / (x1 - x2);
            N = y1 - M * x2;
        }

        public double[,] FindIntersection(Function function)
        {
            double[,] intersection = new double[1,2];
            var m1 = M;
            var m2 = function.M;
            var n1 = N;
            var n2 = function.N;
            
            if (M == function.M)
            {
                Logger.ConsoleWriteLine($"No Intersection of y1 = {M}x + {N} and y2 = {function.M}x + {function.N} found", Logger.MessageTypes.Error);
                intersection = null;
            }
            else
            {
                if (n1 > 0)
                {
                    n2 = n2 - n1;
                }
                else
                {
                    n2 = n2 + n1;
                }

                if (m2 > 0)
                {
                    m1 = m1 - m2;
                }
                else
                {
                    m1 = m1 + m2;
                }

                intersection[0, 0] = n2 / m1;
                intersection[0, 1] = M * intersection[0, 0] + N;
            } 
            return intersection;
        }
        
        public double CalculateY(double x)
        {
            var y = M * x + N;
            return y;
        }
    }
    
}