using System;
using System.Diagnostics;

namespace TwoDimensionalRaycasting
{
    public static class Logger
    {
        /// <summary>
        /// A class for logging console and debug messages with formatting and coloring.
        /// </summary>
        public enum MessageTypes
        {
            Info,
            Warning,
            Error,
            Fatal
        }

        public static void ConsoleWriteLine(string message, MessageTypes messageType)
        {
            switch (messageType)
            {
                case MessageTypes.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[  Info   ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Debug.WriteLine($"[  Info   ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Console.ResetColor();
                    break;
                case MessageTypes.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"[ Warning ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");;
                    Debug.WriteLine($"[ Warning ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Console.ResetColor();
                    break;
                case MessageTypes.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"[  Error  ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Debug.WriteLine($"[  Error  ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Console.ResetColor();
                    break;
                case MessageTypes.Fatal:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"[  Fatal  ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Debug.WriteLine($"[  Fatal  ][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Console.ResetColor();
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[Info][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Debug.WriteLine($"[Info][{DateTime.Now.ToString("hh:mm:ss")}]: {message}");
                    Console.ResetColor();
                    break;
            }
        }
    }
}