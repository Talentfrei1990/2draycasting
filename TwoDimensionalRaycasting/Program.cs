﻿using System;
using System.Collections.Generic;

namespace TwoDimensionalRaycasting
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            #region Debug
            Logger.ConsoleWriteLine("Info", Logger.MessageTypes.Info);
            Logger.ConsoleWriteLine("Warning", Logger.MessageTypes.Warning);
            Logger.ConsoleWriteLine("Error", Logger.MessageTypes.Error);
            Logger.ConsoleWriteLine("Fatal", Logger.MessageTypes.Fatal);
            Function func1 = new Function(1.0, 1.0, 2.0, 2.0);
            Function func2 = new Function(1.0, 2.0, 2.0, 3.0);
            func1.FindIntersection(func2);
            #endregion
            
            Console.Write("\n\n");

            #region Obstacles
            var obstacleNumber = ConsoleInput.Int("Number of obstacles: ");
            Console.Write("\n\n");
            List<Function> obstacleList = new List<Function>();
            for (int i = 0; i < obstacleNumber; i++)
            {
                var obstacleStartX = ConsoleInput.Double($"X-Position of Obstacle-Start({i + 1}): ");
                var obstacleStartY = ConsoleInput.Double($"Y-Position of Obstacle-Start({i + 1}): ");
                var obstacleEndX = ConsoleInput.Double($"X-Position of Obstacle-End({i + 1}): ");
                var obstacleEndY = ConsoleInput.Double($"Y-Position of Obstacle-End({i + 1}): ");
                
                Function tempFunction= new Function(obstacleStartX, obstacleStartY, obstacleEndX, obstacleEndY);
                obstacleList.Add(tempFunction);
                Console.Write("\n");
            }
            #endregion

            Console.Write("\n");
            
            #region Ray-Emitter
            var rayEmitterX = ConsoleInput.Double("X-Position of Ray-Emitter: ");
            var rayEmitterY = ConsoleInput.Double("Y-Position of Ray-Emitter: ");
            var rayEmitterAngle = ConsoleInput.Double("Angle of Ray-Emitter: ");
            var rayEmitterFov = ConsoleInput.Double("FOV of Ray-Emitter: ");
            var rayEmitterRayNumber = ConsoleInput.Int("Number of Rays emitted by Ray-Emitter: ");

            RayEmitter emitter = new RayEmitter(rayEmitterX, rayEmitterY, rayEmitterAngle, rayEmitterFov, rayEmitterRayNumber);

            var rayIndex = new int();
            foreach (var ray in emitter.Rays)
            {
                Logger.ConsoleWriteLine($"Ray({rayIndex}) --> {ray.ToString()}", Logger.MessageTypes.Info);
                rayIndex++;
            }
            #endregion
        }
    }
}