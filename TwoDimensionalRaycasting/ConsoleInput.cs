using System;
using System.Diagnostics;

namespace TwoDimensionalRaycasting
{
    public static class ConsoleInput
    {
        /// <summary>
        /// A class that is used to write a certain message, get Console input, check whether the given input has a certain datatype, output that input if it is the right datatype and write a log message if it is not.
        /// </summary>
        public static int Int(string message)
        {
            var variable = new int();
            Console.Write(message);
            try
            {
                variable = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Logger.ConsoleWriteLine("The given value is not an integer", Logger.MessageTypes.Error);
                Debug.WriteLine(e);
            }

            return variable;
        }

        public static double Double(string message)
        {
            var variable = new double();
            Console.Write(message);
            try
            {
                variable = Convert.ToDouble(Console.ReadLine());
            }
            catch (Exception e)
            {
                Logger.ConsoleWriteLine("The given value is not a double", Logger.MessageTypes.Error);
                Debug.WriteLine(e);
            }

            return variable;
        }
    }
}